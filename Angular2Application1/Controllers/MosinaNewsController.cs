﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Angular2Application1.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Angular2Application1.Controllers
{
    [Route("api/[controller]")]
    public class MosinaNewsController : Controller
    {

        IMosinaNewsRepository _repo;

        public MosinaNewsController(IMosinaNewsRepository repo) {
            _repo = repo;
        }



        // GET: api/values
        [HttpGet("[action]")]
        public IEnumerable<MosinaNews> Get()
        {
            return _repo.SelectAll();
        }
        [HttpGet("[action]")]
        public PagedResult<MosinaNews> GetLimited(int pageNo = 1, int pageSize = 5)
        {
            List<MosinaNews> mosinaNews = _repo.SelectAllByPage(pageNo, pageSize).ToList();
            PagedResult<MosinaNews> newPagedResult = new PagedResult<MosinaNews>(mosinaNews, pageNo, pageSize, mosinaNews.Count, _repo.SelectAll().ToList().Count);

            return newPagedResult;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
