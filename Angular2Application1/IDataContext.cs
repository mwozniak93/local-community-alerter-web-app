﻿using Microsoft.EntityFrameworkCore;

namespace Angular2Application1.Models
{
    public interface IDataContext
    {
        DbSet<MosinaNews> MosinaNews { get; set; }
        DbSet<ZUKNews> ZukNews { get; set; }
    }
}