﻿
import { Observable } from 'rxjs/Observable';
import { PagedResult } from '../models/PagedResult';
export interface IPaggingService<T> {
    pagedResult: PagedResult<T>;
    NextPage(pageNo : number): Observable<PagedResult<T>>;
    //PreviousPage(): Observable<PagedResult<T>>;
}