﻿interface INews {
    id: number;
    title: string;
    message: string;
}