﻿export class PagedResult<T> {
    constructor( pageSize:number, pageNo:number ) {
        //this.Data = data;
        this.pageCount = 0;
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.totalRecordCount = 0;
    }
    pageCount: number;
    pageSize: number;
    pageNo:number;
    totalRecordCount: number;
    maxPageNo: number;
    data: T[];
}