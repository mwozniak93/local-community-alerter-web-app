﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UniversalModule } from 'angular2-universal';
import { MdlModule } from 'angular2-mdl'; 
import { HomeComponent } from '../components/home/home.component';
import { FormsModule } from '@angular/forms';
//import { AppComponent } from '../components/app/app.component'
import { MosinaNewsComponent } from '../components/MosinaNews/mosinanews.component';
import { NewsContentComponent } from '../components/NewsContent/NewsContent.component';
import { NewsDetails } from '../components/GenericModalNewsDetails/NewsDetails.component';
import { DropdownComponent } from '../components/Dropdown/dropdown.component';
import { MosinaNewsContainer } from '../components/MosinaNewsContainer/MosinaNewsContainer.component';
import { PaginationForListComponent } from '../components/Pagination/PaginationForList.component';
@NgModule({
    //bootstrap: [AppComponent],
    declarations: [
        //AppComponent,
        HomeComponent,
        MosinaNewsComponent,
        MosinaNewsContainer,
        NewsDetails,
        DropdownComponent,
        PaginationForListComponent,
        NewsContentComponent
    ],
    imports: [
        UniversalModule, // Must be first import. This automatically imports BrowserModule, HttpModule, and JsonpModule too.
        MdlModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: MosinaNewsContainer },
            //{ path: 'counter', component: CounterComponent },
            //{ path: 'fetch-data', component: FetchDataComponent },
            { path: 'mosina-news', component: MosinaNewsContainer },
            { path: '**', redirectTo: 'home' }
        ])
    ]
})
export class NewsModule {
}
