import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UniversalModule } from 'angular2-universal';
import { AppComponent } from './components/app/app.component'
import { NewsModule } from './modules/News.module'
import { NavMenuComponent } from './components/navmenu/navmenu.component';

//import { HomeComponent } from './components/home/home.component';
//import { FetchDataComponent } from './components/fetchdata/fetchdata.component';
//import { CounterComponent } from './components/counter/counter.component';
//import { MosinaNewsComponent } from './components/MosinaNews/mosinanews.component';
//import { ToggleComponent } from './components/toggle/toggle.component';
//import { PaginationForListComponent } from './components/Pagination/PaginationForList.component';
import { MdlModule} from 'angular2-mdl'; 
@NgModule({
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,
        NavMenuComponent,
        //CounterComponent,
        //FetchDataComponent,
        //HomeComponent,
        //ToggleComponent,
        //PaginationForListComponent,
        //MosinaNewsComponent
    ],
    imports: [
        UniversalModule, // Must be first import. This automatically imports BrowserModule, HttpModule, and JsonpModule too.
        NewsModule,
        MdlModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            //{ path: 'home', component: HomeComponent },
            //{ path: 'counter', component: CounterComponent },
            //{ path: 'fetch-data', component: FetchDataComponent },
            //{ path: 'mosina-news', component: MosinaNewsComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ]
})
export class AppModule {
}
