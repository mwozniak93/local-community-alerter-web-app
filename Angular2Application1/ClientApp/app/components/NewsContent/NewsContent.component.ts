﻿import { Component, Output, Input, ViewEncapsulation, EventEmitter } from '@angular/core';
import { BaseNews } from '../../models/BaseNews';
import { MosinaNews } from '../../models/MosinaNews';
@Component({
    selector: 'news-content',
    template: require('./NewsContent.component.html')

})
export class NewsContentComponent {
    @Input() model: BaseNews;

}