﻿import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { PagedResult } from '../../models/PagedResult';
@Component({
    selector: 'pagination-list',
    template: require('./PaginationForList.component.html')
})

//providers: [{ provide: 'CoursesServiceInterface', useClass: CoursesServiceMock }]
//and inject it like

//constructor(@Inject('CoursesServiceInterface') private coursesService: CoursesServiceInterface) { }


export class PaginationForListComponent  {

   @Input() paginationPagedResult: PagedResult<any>;
    constructor() {
       // this.paginationPagedResult = new PagedResult<any>(0, 3);
    };
    @Output() eChangePage: EventEmitter<PagedResult<any>> = new EventEmitter<PagedResult<any>>();

    NextPage() {
        if (this.paginationPagedResult.pageNo >= this.paginationPagedResult.maxPageNo) {
            return;
        }
        //console.log(this.paginationPagedResult);
        this.paginationPagedResult.pageNo += 1;
        this.eChangePage.emit(this.paginationPagedResult);
    }

    PreviousPage() {
        if (this.paginationPagedResult.pageNo <= 1) {
            return;
        }
        this.paginationPagedResult.pageNo -= 1;
        this.eChangePage.emit(this.paginationPagedResult);
    }
}