﻿import { Component, Output, Input, ViewEncapsulation, EventEmitter } from '@angular/core';
import { MosinaNews } from '../../models/MosinaNews';
import { MosinaNewsComponent } from '../MosinaNews/mosinanews.component';
import { NewsDetails } from '../GenericModalNewsDetails/NewsDetails.component';
import { MosinaNewsService } from '../../services/MosinaNewsService';
import { Router } from '@angular/router';
import { PagedResult } from '../../models/PagedResult';
import { PaginationForListComponent } from '../Pagination/PaginationForList.component';
@Component({
    selector: 'drop-down',
    template: require('./Dropdown.component.html')
})
export class DropdownComponent {
    @Output() eDropdownChange: EventEmitter<any> = new EventEmitter();
    @Input() initOptions: Array<any>;
    @Input() modelValue: number;
}