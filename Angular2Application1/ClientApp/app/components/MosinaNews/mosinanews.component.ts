﻿import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
// Observable class extensions
import 'rxjs/add/observable/of';
// Observable operators
import 'rxjs/add/operator/catch';
//import { Http, Response } from '@angular/http';
import { MosinaNewsService } from '../../services/MosinaNewsService';
import { MosinaNews } from '../../models/MosinaNews';
import { PagedResult } from '../../models/PagedResult';
import { MdlDialogService } from 'angular2-mdl';
//import { PaggingService } from '../../services/PaggingService';
//import { PaginationForListComponent } from '../Pagination/PaginationForList.component';
@Component({
    selector: 'mosina-news',
    template: require('./mosinanews.component.html'),
    providers: [MosinaNewsService]
})
export class MosinaNewsComponent implements OnInit  {
    @Output() eMosinaNewsSelected: EventEmitter<MosinaNews> = new EventEmitter();
    @Input() currentMosinaNews: MosinaNews; 
   // @Input() selectedMosinaNews: MosinaNews; 
    constructor(
        private mosinaNewsService: MosinaNewsService,
        private router: Router, private dialogService: MdlDialogService) {
    }

    setCurrentNews(mosinaNews : MosinaNews) {
        this.eMosinaNewsSelected.emit(mosinaNews);
        this.dialogService.alert(mosinaNews.message);
    }
  

    ngOnInit() {
       

    }

}