﻿import {
    Component, Input,
    trigger, state, animate, transition, style
} from '@angular/core';

@Component({
    selector: 'toggle',
    template: `
    <div class="" [@toggleState]="shouldToggle">
		    <ng-content></ng-content>
		</div>
    `,
    animations: [
        trigger('toggleState', [
            state('true', style({height:200, width:400})),
            state('false', style({ maxHeight: 0, padding: 0, display: 'none' })),
            transition('* => *', animate('300ms')),
        ])
    ],
})
export class ToggleComponent {
    @Input() shouldToggle = false;
}