import { Component } from '@angular/core';
import { MdlLayoutComponent } from 'angular2-mdl'; 
@Component({
    selector: 'app',
    template: require('./app.component.html'),
    styles: [require('./app.component.css')],
    providers: [MdlLayoutComponent]
})
export class AppComponent {

}
