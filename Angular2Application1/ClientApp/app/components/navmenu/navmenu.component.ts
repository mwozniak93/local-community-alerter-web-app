import { Component } from '@angular/core';
import { MdlLayoutComponent } from 'angular2-mdl'; 
@Component({
    selector: 'nav-menu',
    template: require('./navmenu.component.html'),
    styles: [require('./navmenu.component.css')],
    providers: [MdlLayoutComponent]
})
export class NavMenuComponent {
}
