import { Component } from '@angular/core';

@Component({
    selector: 'home',
    template: require('./home.component.html')
})
export class HomeComponent {
    toggle = false;
    handleClick() {
        this.toggle = !this.toggle
        console.log(this.toggle);
    }
}
