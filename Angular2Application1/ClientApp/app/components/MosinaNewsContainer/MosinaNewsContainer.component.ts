﻿import { Component, Output, Input, ViewEncapsulation, EventEmitter } from '@angular/core';
import { MosinaNews } from '../../models/MosinaNews';
import { MosinaNewsComponent } from '../MosinaNews/mosinanews.component';
import { NewsDetails } from '../GenericModalNewsDetails/NewsDetails.component';
import { NewsContentComponent } from '../NewsContent/NewsContent.component';
import { DropdownComponent } from '../Dropdown/Dropdown.component';
import { MosinaNewsService } from '../../services/MosinaNewsService';
import { Router } from '@angular/router';
import { PagedResult } from '../../models/PagedResult';
import { PaginationForListComponent } from '../Pagination/PaginationForList.component';
@Component({
    selector: 'mosinanews-container',
    template: require('./MosinaNewsContainer.component.html'),
    providers: [MosinaNewsService]
})
export class MosinaNewsContainer {
    selectedNews: MosinaNews;
    @Output() eNewsClicked: EventEmitter<MosinaNews> = new EventEmitter();
    pageDisplayOptions: Array<number> = [
        1,2,3,4,5
    ];

    onNewsChange(mosinaNews : MosinaNews) {
        this.selectedNews = mosinaNews;
        console.log(this.selectedNews);
        this.eNewsClicked.emit(mosinaNews);
    }
    mosinaNews: MosinaNews[];
    pagedResult: PagedResult<MosinaNews>;


    constructor(
        private mosinaNewsService: MosinaNewsService,
        private router: Router) {
        this.pagedResult = new PagedResult<MosinaNews>(0, 3);
        this.reloadData();
    }
    reloadData() {
        this.mosinaNewsService.NextPage(1).subscribe((res: any) => {
            this.pagedResult = res;
            this.mosinaNews = res.data;
            //console.log(this.mosinaNewsService.pagedResult);
        });
    }

    onPageChange(_pagedResult: PagedResult<any>) {
        //console.log(_pagedResult);
        this.NextPage();
    }
    //onPagePrevious(_pagedResult: PagedResult<any>) {
    //    console.log(_pagedResult);
    //    this.PreviousPage();
    //}
    onDropdownChange(pageSize: number) {

        this.mosinaNewsService.pagedResult.pageSize = pageSize;
        //this.mosinaNewsService.pagedResult.pageNo -= 1;
      

        this.reloadData();
    }
    public NextPage() {
        //if (this.pagedResult.pageNo >= this.pagedResult.maxPageNo) {
        //    return;
        //}
        this.mosinaNewsService.NextPage(this.pagedResult.pageNo).subscribe((res: any) => {
            this.mosinaNews = res.data;
            this.pagedResult = res;

        });
    }
    //public PreviousPage() {
    //    //if (this.pagedResult.pageNo <= 1) {
    //    //    return;
    //    //}
    //    this.mosinaNewsService.PreviousPage().subscribe((res: any) => {
    //        //alert(res);
    //        console.log(res);
    //        this.mosinaNews = res.data;
    //        this.pagedResult = res;
    //    });
    //}
   // @Input() customers: Customer[];
}