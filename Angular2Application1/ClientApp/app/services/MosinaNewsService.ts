﻿import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { GenericService } from './GenericService';
import { MosinaNews } from '../models/MosinaNews';
@Injectable()
export class MosinaNewsService extends GenericService<MosinaNews>   {
   
    constructor(http: Http) {
        super(http);
        this.UrlPath = "/api/MosinaNews/";
    }

}