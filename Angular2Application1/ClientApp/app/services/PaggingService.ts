﻿import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { IPaggingService } from '../interfaces/IPagingService';
import { PagedResult } from '../models/PagedResult';
@Injectable()
export class PaggingService<T>   {

    constructor(private http: Http) {
        this.pagedResult = new PagedResult<T>(1, 3);
        //this.pagedResult = pageRes;
        //this.NextPage();
        //console.log(this.pagedResult);
    }
    public pagedResult: PagedResult<T>;
    
    UrlPath: string;
    NextPage(): Observable<PagedResult<T>> {
        console.log(this.UrlPath + "GetLimited?pageSize=" + "3" + "&pageNo=" + "1");
       // .get(this.UrlPath + "GetLimited?pageSize=" + this.pagedResult.PageSize + "&pageNo=" + this.pagedResult.PageNo)
        return this.http
            .get(this.UrlPath + "GetLimited?pageSize=" + "3" + "&pageNo=" + "1")
            .map(this.extractData);
    }
    
    private extractData(res: Response) {
        let body = res.json();
        console.log(body);
        return body || {};
    }
}