﻿
import { Injectable } from '@angular/core';
import { Http, Response  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { PagedResult } from '../models/PagedResult';
import 'rxjs/add/operator/map';
import { IPaggingService } from '../interfaces/IPagingService';
@Injectable()
export class GenericService<T>   implements IPaggingService<T> {
    constructor(private http: Http) {
        this.pagedResult = new PagedResult<T>(3, 0);
        //console.log(this.pagedResult);
    }
    pagedResult: PagedResult<T>;
        //pagedResult: PagedResult<T>;
    UrlPath: string;
    NextPage(pageNo:number): Observable<PagedResult<T>> {

        this.pagedResult.pageNo = pageNo;
        //console.log(this.UrlPath);
        return this.http
            .get(this.UrlPath + "GetLimited?pageSize=" + this.pagedResult.pageSize + "&pageNo=" + this.pagedResult.pageNo)
            .map(this.extractData);
    }

    //PreviousPage(): Observable<PagedResult<T>> {
    //    this.pagedResult.pageNo -= 1;
    //    //console.log(this.UrlPath);
    //    return this.http
    //        .get(this.UrlPath + "GetLimited?pageSize=" + this.pagedResult.pageSize + "&pageNo=" + this.pagedResult.pageNo)
    //        .map(this.extractData);
    //}
    private extractData(res: Response) {
        let body = res.json();
        
        //console.log(body);
        return body || {};
    }


    //NextPage(): Observable<PagedResult<T>>  {
    //    return this.http
    //        .get(this.UrlPath + "GetLimited?pageSize=" + this.pagedResult.PageSize + "&pageNo=" + this.pagedResult.PageNo)
    //        .map(this.extractData);
    //}
    

    //GetAllPaged(execApiPath:string): Observable<T> {
    //    return this.http
    //        .get(this.UrlPath + execApiPath)
    //        .map(this.extractData);
    //}
    //private extractData(res: Response) {
    //    let body = res.json();
    //    console.log(body);
    //    return body || {};
    //}
}