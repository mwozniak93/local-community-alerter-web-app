﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Angular2Application1.Models
{
    public class DataContext : IdentityDbContext<AppUser>
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }
        public DbSet<MosinaNews> MosinaNews { get; set; }
        public DbSet<ZUKNews> ZukNews { get; set; }
 
        //public DbSet<User> Users { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AppUser>(entity =>
            {
                entity.ToTable(name: "Users", schema: "Security");
                entity.Property(e => e.Id).HasColumnName("UserId");

            });
            modelBuilder.Entity<AppRole>().ToTable("AspNetRoles", "security");
            //modelBuilder.Entity<CustomUserClaim>().ToTable("AspNetUserClaims", "security");
            //modelBuilder.Entity<CustomUserLogin>().ToTable("AspNetUserLogins", "security");
            //modelBuilder.Entity<AppRole>().ToTable("AspNetUserRoles", "security");
            //modelBuilder.Entity<AppUser>().ToTable("User");
            modelBuilder.Entity<MosinaNews>().ToTable("MosinaNews");
            modelBuilder.Entity<ZUKNews>().ToTable("ZukNews");
     
        }

    }
}
