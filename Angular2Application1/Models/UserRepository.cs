﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angular2Application1.Models
{
    public class UserRepository : Repository<AppUser, DataContext>, IUsersRepository
    {
        public UserRepository(DataContext context) : base(context)
        {
        }
    }
}
