﻿using Angular2Application1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angular2Application1.Models
{
    public interface IUsersRepository: IRepository<AppUser>
    {
    }
}
