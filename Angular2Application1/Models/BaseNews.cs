﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Angular2Application1.Models
{
    public abstract class BaseNews : BaseEntity
    {

        public string Message { get; set; }
        public string Title { get; set; }
    }
}
