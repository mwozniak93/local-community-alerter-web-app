﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Angular2Application1.Models
{

    public class AppUser : IdentityUser, IEntity
    {
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //public int Id { get; set; }

        //public string Username { get; set; }
        //public string Password { get; set; }
        public string Firstname { get; set; }

        int IEntity.Id { get { return int.Parse(Id); } set { } }

       
    }
}
