﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angular2Application1.Models
{
    public interface IRepository<T>
    {
        IEnumerable<T> SelectAll();
        T SelectByID(int id);
        void Insert(T obj);
        void Update(T obj);
        void Delete(int id);
        void Save();
        IEnumerable<T> SelectAllByPage(int pageNo, int pageSize);

    }
}
