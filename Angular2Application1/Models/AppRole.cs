﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angular2Application1.Models
{
    public class AppRole : IdentityRole
    {
        public AppRole(string roleName) : base(roleName)
        {
        }
    }
}
