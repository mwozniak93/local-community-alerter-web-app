﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angular2Application1.Models
{
    public class PagedResult<T>
    {

        public int PageNo { get; set; }

        public int PageSize { get; set; }

        public int PageCount { get; set; }

        public long TotalRecordCount { get; set; }
        public int MaxPageNo { get; set; }
        public List<T> Data { get; private set; }
        public PagedResult(IEnumerable<T> items, int pageNo, int pageSize, long totalRecordCount, int maxPageNo)
        {
            Data = new List<T>(items);

            PageNo = pageNo;
            PageSize = pageSize;
            TotalRecordCount = totalRecordCount;
            PageCount = totalRecordCount > 0 ? (int)Math.Ceiling(totalRecordCount / (double)pageSize) : 0;
            MaxPageNo = maxPageNo;
        }

        public PagedResult()
        {
        }
    }
}
