﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Angular2Application1.Models
{
    public class Repository<T, DBC> : IRepository<T> where T:class, IEntity where DBC : DbContext
    {
        //protected readonly DBC Context;
        protected DbSet<T> DbSet;
        private DBC Context;
        public Repository(DBC context) {
            Context = context;
            DbSet = Context.Set<T>();
        }
        public IEnumerable<T> SelectAllByPage(int pageNo, int pageSize) {
            int skip = (pageNo - 1) * pageSize;
            int total = DbSet.Count();
            var result = DbSet
                .OrderBy(c => c.Id)
                .Skip(skip)
                .Take(pageSize)
                .ToList();
            return result;
        }

        public IEnumerable<T> SelectAll()
        {
            return DbSet.ToList();
        }
        public T SelectByID(int id)
        {
            return DbSet.First(x=>x.Id==id);
        }
        public void Insert(T obj)
        {
            DbSet.Add(obj);
        }
        public void Update(T obj)
        {
            DbSet.Attach(obj);
           // DbSet.Entry(obj).State = EntityState.Modified;
        }
        public void Delete(int id)
        {
            //T existing = Context.f.Find(id);
            //DbSet.Remove(existing);
        }
        public void Save()
        {
            Context.SaveChanges();
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {

            if (!this.disposed)
                if (disposing)
                    Context.Dispose();

            this.disposed = true;
        }
        public void Dispose()
        {

            Dispose(true);

        }

    }
}
