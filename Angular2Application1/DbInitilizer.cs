﻿using Angular2Application1.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Angular2Application1
{

    public enum Roles { Admin, MosinaNewsAdmin, ZUKNewsAdmin }
    public static class DbInitilizer
    {
        public static async Task Init(DataContext context, UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            //context.Database.
            context.Database.EnsureCreated();

            if (!context.Users.Any())
            {



                var password = new PasswordHasher<AppUser>();
                AppUser user = new AppUser() { Firstname = "Mateusz", UserName = "kunmateo93", };
                user.PasswordHash = password.HashPassword(user, "ziomal123");

                await SeedRoles(roleManager, userManager, user, context);

                await userManager.CreateAsync(user);
                await context.SaveChangesAsync();
                //Task.Run(async () => {
                //    await roleManager.CreateAsync(adminRole);
                //    await userManager.AddToRoleAsync(user, "Admin");

                //context.SaveChangesAsync();
                //});


                // user.Roles.Add(new Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<int>() { RoleId = 1, UserId = user.Id });

            }

            if (!context.MosinaNews.Any())
            {


                var oMosinaNews = new List<MosinaNews>() {
                new MosinaNews() {  Message =" W dniu 18.10.2017 odbędzie festyn na rynku w Mosinie! Serdecznie zapraszamy wszystkich zainteresowanych!", Title = "Festyn - 18.10.2017"},
                new MosinaNews() {  Message ="Odnotowalismy spory spadek poziomu edukacyjnego!1", Title = "Edukacja1"},
                new MosinaNews() {  Message ="Odnotowalismy spory spadek poziomu edukacyjnego!2", Title = "Edukacja2"},
                new MosinaNews() {  Message ="Odnotowalismy spory spadek poziomu edukacyjnego!3", Title = "Edukacja3"},
                new MosinaNews() {  Message ="Odnotowalismy spory spadek poziomu edukacyjnego!4", Title = "Edukacja4"},
                new MosinaNews() {  Message ="Odnotowalismy spory spadek poziomu edukacyjnego!5", Title = "Edukacja5"},
                new MosinaNews() {  Message ="Odnotowalismy spory spadek poziomu edukacyjnego!6", Title = "Edukacja6"},
                new MosinaNews() {  Message ="Odnotowalismy spory spadek poziomu edukacyjnego!7", Title = "Edukacja7"},
                new MosinaNews() {  Message ="Odnotowalismy spory spadek poziomu edukacyjnego!8", Title = "Edukacja8"},
            };
                foreach (var oNews in oMosinaNews)
                {
                    context.MosinaNews.Add(oNews);
                }
                context.SaveChanges();
            }

            if (!context.ZukNews.Any())
            {
                var oZUKNews = new ZUKNews()
                {
                    Message = "Some ZUK Info 1",
                    Title = "Lorem",
                    AlertDates = new DateTime(2017, 5, 20).ToString() + ";" + new DateTime(2017, 1, 10).ToString() + ";" + new DateTime(2017, 2, 20).ToString() + ";",

                };
                var oZUKNews2 = new ZUKNews()
                {
                    Message = "Some ZUK Info 2",
                    Title = "Lorem",
                    AlertDates = (new DateTime(2017, 7, 21)).ToString() + ";" + new DateTime(2017, 3, 14).ToString() + ";",
                };
                context.ZukNews.Add(oZUKNews);
                context.ZukNews.Add(oZUKNews2);
                context.SaveChanges();
            }


        }
        public static async Task SeedRoles(RoleManager<AppRole> roleManager, UserManager<AppUser> userManager, AppUser user, DataContext context)
        {

            foreach (var role in Enum.GetNames(typeof(Roles)))
            {
                //if (!await roleManager.RoleExistsAsync(role))
                //{
                var _role = new AppRole(role);
          
                     await roleManager.CreateAsync(_role);
             
                if (role == "Admin")
                {
             
                         await userManager.AddToRoleAsync(user, role);
               
                    context.SaveChanges();
                    // user.Roles.Add(_role);
                }
                //}
            }
            context.SaveChanges();
        }
    }
}
